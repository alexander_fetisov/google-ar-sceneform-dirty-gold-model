/*
 * Copyright 2018 Google LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.ar.sceneform.dirtygoldmodel;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Texture;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.util.Objects;

/**
 * This is an example activity that uses the Sceneform UX package to make common AR tasks easier.
 */
public class ArActivity extends AppCompatActivity {

    private static final String TAG = ArActivity.class.getSimpleName();
    private static final double MIN_OPENGL_VERSION = 3.1;

    private ArFragment arFragment;
    private ModelRenderable andyRenderable;

    private EditText mEditText;
    private ProgressBar mProgressBar;
    private Button mButton;
    private TextView mNameText;

    private int count = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkIsSupportedDeviceOrFinish(this)) {
            return;
        }

        setContentView(R.layout.activity_ux);

        mButton = findViewById(R.id.btn_upload);
        mEditText = findViewById(R.id.edit_text);
        mProgressBar = findViewById(R.id.progress_bar);
        mNameText = findViewById(R.id.nameModel);

        mButton.setOnClickListener(v -> {
            String url = mEditText.getText().toString().trim();
            if (url.isEmpty()) {
                Toast.makeText(this, "Url can not be empty", Toast.LENGTH_SHORT).show();
            } else {
                mButton.setEnabled(false);
                mProgressBar.setVisibility(View.VISIBLE);
                uploadModel(url);
            }
        });

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);

        arFragment.setOnTapArPlaneListener(
                (HitResult hitResult, Plane plane, MotionEvent motionEvent) -> {
                    if (andyRenderable == null) {
                        Toast.makeText(this, "Download the model!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    // Create the Anchor.
                    Anchor anchor = hitResult.createAnchor();
                    AnchorNode anchorNode = new AnchorNode(anchor);
                    anchorNode.setParent(arFragment.getArSceneView().getScene());

                    anchorNode.setName("Andy #" + count);

                    count++;

                    mNameText.setText(anchorNode.getName());

                    // Create the transformable andy and add it to the anchor.
                    TransformableNode andy = new TransformableNode(arFragment.getTransformationSystem());

                    andy.getScaleController().setEnabled(false);

                    andy.setParent(anchorNode);
                    andy.setRenderable(andyRenderable);
                    andy.select();
                });
    }

    //"https://vk.com/doc281205072_472110320"
    private void uploadModel(String url) {
        ModelRenderable.builder().setSource(this, Uri.parse(url))
                .build()
                .thenAccept(renderable -> {
                    mButton.setEnabled(true);
                    mProgressBar.setVisibility(View.GONE);

                    andyRenderable = renderable;

                    Texture.builder().setSource(this, R.drawable.gold_cc).build()
                            .thenAccept(texture -> andyRenderable.getMaterial().setTexture("base", texture));

                    Texture.builder().setSource(this, R.drawable.gold_ao).build()
                            .thenAccept(texture -> andyRenderable.getMaterial().setTexture("ambientOcclusion", texture));

                    Texture.builder().setSource(this, R.drawable.gold_metal).build()
                            .thenAccept(texture -> andyRenderable.getMaterial().setTexture("metal", texture));

                    Texture.builder().setSource(this, R.drawable.gold_roughness).build()
                            .thenAccept(texture -> andyRenderable.getMaterial().setTexture("roughness", texture));

                    Texture.builder().setSource(this, R.drawable.gold_normal).build()
                            .thenAccept(texture -> andyRenderable.getMaterial().setTexture("normal", texture));
                })
                .exceptionally(
                        throwable -> {
                            Toast toast = Toast.makeText(this,
                                    "Unable to load andy renderable", Toast.LENGTH_LONG);

                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            mButton.setEnabled(true);
                            mProgressBar.setVisibility(View.GONE);
                            return null;
                        });
    }

    public static boolean checkIsSupportedDeviceOrFinish(final Activity activity) {
        String openGlVersionString =
                ((ActivityManager) Objects.requireNonNull(activity.getSystemService(Context.ACTIVITY_SERVICE)))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();

        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.1 later");
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.1 or later", Toast.LENGTH_LONG)
                    .show();

            activity.finish();
            return false;
        }

        return true;
    }

}